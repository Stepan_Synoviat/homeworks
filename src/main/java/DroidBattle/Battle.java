package DroidBattle;

import java.util.Scanner;

public class Battle {


	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Player p1 = new Player("Igor");
		p1.getPlayerStats();
		System.out.println();
		Player p2 = new Player("Vasa");
		p2.getPlayerStats();
		RandomNmbr random = new RandomNmbr();


		while (p1.getHealth() > 0 && p2.getHealth() > 0) // Both Players Alive?
		{
			int turnCounter = 1;
			while (turnCounter % 2 != 0 && p1.getHealth() > 0)  // Player 1 Turn
			{
				System.out.println("--- Player 1 Info ---");
				System.out.println();

				p1.getPlayerStats();

				System.out.println();
				System.out.println("Player 1... what would you like to do?");
				System.out.println();

				int tossAction = input.nextInt();
				System.out.println();

				switch (tossAction) {
					default:
						System.out.println("Command not recognized!");
						System.out.println("Skipping your turn!");
						break;
					case 1:
						tossAction = 1;
						int randDamage = random.getRandomNumr();
						System.out.println(randDamage);
						p2.takeDamage(randDamage);
						break;
					case 2:
						tossAction = 2;
						int randDefense = random.getRandom();
						System.out.println(randDefense);
						p1.takeDefense(randDefense);
						break;
				}
				turnCounter++;
			}

			p2.setDeffense(3);
			p2.setDamage(5);
			System.out.println();

			while (turnCounter % 2 == 0 && p2.getHealth() > 0)  // Player 2 Turn
			{
				System.out.println("--- Player 2 Info ---");
				System.out.println();

				p2.getPlayerStats();

				System.out.println();
				System.out.println("Player 2... what would you like to do?");
				System.out.println();

				int tossAction = input.nextInt();
				System.out.println();

				switch (tossAction) {
					default:
						System.out.println("Command not recognized!");
						System.out.println("Skipping your turn!");
						break;
					case 1:
						tossAction = 1;
						int randDamage = random.getRandomNumr();
						System.out.println(randDamage);
						p1.takeDamage(randDamage);
						break;
					case 2:
						tossAction = 2;
						int randDefense = random.getRandom();
						System.out.println(randDefense);
						p2.takeDefense(randDefense);
						break;
				}
				turnCounter++;
				p1.setDeffense(3);
				p1.setDamage(5);
			}


				if (p1.getHealth() <= 0) {
					System.out.println("--- Player 2 is the Winner ---");
					System.out.println();
					System.out.println("--- Player 2 Stats ---");
					System.out.println();
					p2.getPlayerStats();
					System.out.println();

				} else if (p2.getHealth() <= 0) {
					System.out.println("--- Player 1 is the Winner ---");
					System.out.println();
					System.out.println("--- Player 1 Stats ---");
					System.out.println();
					p1.getPlayerStats();
					System.out.println();

				} else {
					System.out.println("Next Round");
				}
			}

			p2.setDeffense(3);
			p2.setDamage(5);
		}
	}


