package DroidBattle;

import java.util.*;

public class Player {
	private String name;
	private int healthPoints = 100;
	private int deffense = 3;
	private int damage = 5;
	RandomNmbr randomX = new RandomNmbr();

	public Player(String name) {
		this.name = name;
	}

	public int getHealth() {
		return healthPoints;
	}

	public void setHealth(int health) {
		this.healthPoints = health;
	}

	public int getDeffense() {
		return deffense;
	}

	public void setDeffense(int deffense) {
		this.deffense = deffense;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int takeDamage(int randDamage) {
		if (randDamage == 0) {
			System.out.println("You miss");
			return healthPoints;
		} else if (randDamage <= 2) {
			healthPoints -= damage * 2 - deffense;
			System.out.println("--- Attacked opponent with " + (damage * 2) + " damage ---");
			return healthPoints;
		} else {
			healthPoints -= damage - deffense;
			System.out.println("--- Attacked opponent with " + damage + " damage ---");
			return healthPoints;
		}
	}

	public int takeDefense(int getRandom) {
		deffense += getRandom;
		System.out.println("Now my defense = " + deffense);
		return deffense;
	}

	public void getPlayerStats() {
		System.out.println("Player name = " + name);
		System.out.println("Health points = " + healthPoints);
		System.out.println("Deffense = " + deffense);
		System.out.println("Damage = " + damage);
	}
}


